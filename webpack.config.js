const webpack = require('webpack');
const path = require('path');

module.exports= {
    context: __dirname + '/app',
    entry: {
        app: './index.js',
        vendor: [
            'active-resource',
            'angular',
            'angular-animate',
            'angular-sanitize',
            'angular-spinner',
            'axios',
            'es6-promise',
            'jquery',
            'jquery-ui',
            'moment',
            'moment-range',
            'qs',
            'occasion-sdk',
            'underscore',
            'underscore.inflection',
            'underscore.string',
            'lodash',
            'angular-moment-picker',
            'angularjs-dropdown-multiselect'
        ]
    },
    output: {
        path: __dirname + '/app',
        filename: '[name].bundle.js'
    },
    resolve: {
        alias: {
          'active-resource': path.join(process.cwd(), 'node_modules', 'active-resource', 'build', 'active-resource.min.js')
        }
    }
}