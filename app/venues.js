//Venue Names and IDs
window.VENUES = [
    { name: "Allen, TX", id: 'fwnyb9ol', key: '5d5752e45acc432fa0557cc553150f8c' },
    { name: "Frisco, TX", id: 'xfxvldtd', key: '38f4dae339a244528893bf4a01cc4b88' },
    { name: "Irving, TX", id: 'ten7skou', key: 'a94ce13a537548a6bfb93d84ae789492' },
    { name: "Lake Highlands, TX", id: 'owqaoywh', key: '14ad6e9a13184f8280b7df9c4507b55d' },
    { name: "Flower Mound, TX", id: 'kxdiiyuy', key: 'a7e4dc6997a743e884b4905d7d09c904' },
    { name: 'Plano, TX', id: 'kzpzuype', key: '57ba792353004ae18afe985380ffb6c1' },
    { name: 'McKinney, TX', id: 'mxRpsqnC', key: '06ef63be093742ad9d24fb2162368c82' }
];