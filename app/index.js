const $ = require('jquery');

angular.module('PlayStreet', ['angularSpinner', 'ngAnimate', 'ngSanitize', 'moment-picker', 'angularjs-dropdown-multiselect'])
  .controller('AppController', function($scope, occasionSDKService) {
    
    //Runs On Init
    this.$onInit = function(){
        console.log("PSM LISTING EXPERIENCE VERSION 0.0.7");
        console.log("Listing Component Init");

        //List View Element Variable
        $scope.listViewElement = document.documentElement;

        //Initialize loading variables
        $scope.loading = true;
        $scope.loadingProducts = true;
        //$scope.multiSelectDisabled = true;
        $scope.loadingMoreDays = false;
        $scope.loadingScroll = false;

        document.getElementById('mainContent').style.display = 'table';

        //Keep track of the first change of the calendar on page load
        $scope.calendarInitialChange = false;
        
        //Set root store URL
        $scope.storeURL = 'https://psm.getoccasion.com/p/n/';

        //Store todays date and set the initial calendar search date
        $scope.today = moment();
        $scope.searchDate = moment();

        $scope.earliestLoaded = moment().startOf('day');
        $scope.latestLoaded = null;

        //Set default values for Merchant, Venue, Product variables
        $scope.venues = [];
        $scope.activeVenues = [];
        $scope.activeProducts = [];
        $scope.masterActiveDays = [];
        $scope.activeDays = [];

        //Multi-Select Configuration
        $scope.multiSelectModel = []; 
        $scope.multiSelectOptions = []; 
        $scope.multiSelectSettings = { 
          smartButtonMaxItems: 2,
          displayProp: 'label',
          //closeOnSelect: true,
          //closeOnDeselect: true
        };
        $scope.multiSelectTranslations = {
          buttonDefaultText: 'Locations...'
        }
        $scope.multiSelectEvents = {
          onSelectionChanged: () => $scope.multiSelectChanged()
        }

        //Load Initial Data
        console.log("Begin loadInitialData");
        $scope.loadInitialData()
          .then( () => {
            //Done loading...
            console.log("Done Loading Initial Data");
            $scope.getActiveProducts();

            $scope.leftTop = $('#left').offset().top;

            angular.element(window).bind('scroll', function(){
              $scope.listViewScrolled();
            });
          });
    };

    $scope.loadInitialData = function(){
      return new Promise( (resolve, reject) => {
        //occasionSDKService.listAllVenues();

        //Get all Venues
        occasionSDKService.getAllVenues()
        .then( venues => {

          //Set Multi-Select Options and Scope Venues
          let count = 0;
          venues.forEach( venue => {
            let toPush = { id: count, label: venue.window_data.name, venue: venue };
            $scope.multiSelectOptions.push(toPush);
            $scope.multiSelectModel.push(toPush);
            $scope.activeVenues.push(toPush);
            count++;
          });

          console.log("Active Venues array with all products", this.activeVenues);
          
          //Remove loading and display mainContent
          $scope.loading = false;
          $scope.$apply();
          
          //Resolve
          resolve();
        });
      })
    }

    $scope.getActiveProducts = function(){
      console.log("Creating Active Products array");
      $scope.loadingProducts = true;
      $scope.activeProducts = [];

      let count = 0;
      let max = $scope.activeVenues.length;

      $scope.activeVenues.forEach( venue => {
        venue.venue.products().perPage(1000).all()
          .then( products => {

            $scope.activeProducts.push( ...products.all().filter( product => (product.status == 'active' /*|| product.status == 'sold_out'*/) && !product.title.toLowerCase().includes("private event") ) );
           /* $scope.activeProducts.filter(product => product.status == "sold_out").forEach(product => {
              console.log("SOLD OUT", product.title, product);
            });*/
            count++;
            if(count == max) $scope.loadInitialDays();
          })
          .catch( error => console.log("Loading products error", error, venue) );
      });
    }

    $scope.loadInitialDays = function(){
      console.log("Loading Initial Days");
      $scope.loadingProducts = true;
      if($scope.activeProducts != null){

        let startDate = new moment().startOf('day');
        let endDate = new moment($scope.searchDate).add(30, 'days').endOf('day');

        console.log("Start date", startDate);
        console.log("End date", endDate);

        $scope.earliestLoaded = new moment(startDate);
        $scope.latestLoaded = new moment(endDate);

        let count = 0;
        let max = $scope.activeProducts.length;
        
        $scope.activeProducts
          .forEach( product => {
            product
              .timeSlots()
              .where({
                status: "bookable",
                startsAt: { 
                  ge: startDate.toDate(), 
                  le: endDate.toDate()
                }
              })
              .all()
              .then( timeSlots => {
                product.timeSlotsArray = timeSlots.all();

                if(product.timeSlotsArray.length > 0){
                  
                  timeSlots.all().forEach( timeSlot => {
                    timeSlot.product = product;
                  });

                  $scope.masterActiveDays.push(...timeSlots.all());
                }else if(product.status == 'sold_out'){
                  $scope.masterActiveDays.push({
                    product: product,
                    sold_out: true
                  });
                }  
                count++;

                //Resolve Condition
                if(count == max) {
                  console.log("Done loading time slots (initial days)");
                  $scope.parseTimeSlots(false);
                }
              }).catch(error => console.log("TIMESLOT ERROR", error) );
        });
      }
    }

    $scope.multiSelectChanged = function(){
      if(!$scope.loading){
        //$scope.multiSelectDisabled = true;
        $scope.activeVenues = [];
        $scope.activeProducts = [];
        $scope.masterActiveDays = [];
        $scope.activeDays = [];
        $scope.multiSelectModel.forEach( venue => {
          $scope.activeVenues.push({ venue: venue.venue });
        });

        //Get products for venues
        if($scope.activeVenues != null && $scope.activeVenues.length != 0){
          $scope.getActiveProducts();
        }else{
          //$scope.multiSelectDisabled = false;
        } 
      }
    }

    $scope.listViewScrolled = function(){
      let doc = document.documentElement;
      let docScrollTop = document.documentElement.scrollTop || document.body.scrollTop || window.pageYOffset;
      let body = document.querySelector(".ListingExperience");
      let left = document.querySelector(".ListingExperience #left");
      let right = document.querySelector(".ListingExperience #right");

      if(doc.clientWidth > 900){
        if( docScrollTop >= ($scope.leftTop) && docScrollTop < (body.scrollHeight + 250) ){
          left.style.position = 'fixed';
          left.style.top = '100px';
          right.style.marginLeft = '30%';
        }else{
          left.style.position = 'relative';
          left.style.top = '0';
          right.style.marginLeft = '0';
        }
      }

      if(docScrollTop >= (body.scrollHeight - 2000)){
        if($scope.loadingMoreDays == false && $scope.loadingProducts == false){
          console.log("Loading More");
          $scope.loadMoreDays();
        }
      }
    }

    $scope.searchDateChanged = function(){
      console.log("Search Date Changed", $scope.searchDate);
      if( moment($scope.searchDate).isBefore($scope.latestLoaded) || moment($scope.searchDate).isSame($scope.latestLoaded) ){
        $scope.scrollToSearchDate();
      }else{
        $scope.loadingScroll = true;
        $scope.loadSpecificDays();
      }
    }

    $scope.loadMoreDays = function(){
      console.log("Load More Days");
      $scope.loadingMoreDays = true;
      $scope.$apply();

      if($scope.activeProducts != null){

        let startDate = new moment($scope.latestLoaded).add(1, 'days').startOf('day');
        let endDate = new moment(startDate).add(7, 'days').endOf('day');

        $scope.latestLoaded = new moment(endDate);

        let count = 0;
        let max = $scope.activeProducts.length;
        $scope.activeProducts.forEach( product => {
          product
            .timeSlots()
            .where({
              status: "bookable",
              startsAt: { 
                ge: startDate.toDate(), 
                le: endDate.toDate()
              }
            })
            .all()
            .then( timeSlots => {
              product.timeSlotsArray.push(...timeSlots.all());
              
              if(product.timeSlotsArray.length > 0){
                
                timeSlots.all().forEach( timeSlot => {
                  timeSlot.product = product;
                });

                $scope.masterActiveDays.push(...timeSlots.all());
              }else if(product.status == 'sold_out'){
                $scope.masterActiveDays.push({
                  product: product,
                  sold_out: true
                });
              }  
              count++;

              //Resolve Condition
              if(count == max) {
                console.log("Done loading time slots");
                $scope.parseTimeSlots(false);
              }
            });
        });
      }
    }

    $scope.loadSpecificDays = function(){
      console.log("Load specific days", $scope.searchDate);

      $scope.loadingMoreDays = true;
      //$scope.$apply();

      if($scope.activeProducts != null){

        let startDate = new moment($scope.latestLoaded).add(1, 'days').startOf('day');
        let endDate = new moment($scope.searchDate).add(7, 'days').endOf('day');

        $scope.latestLoaded = new moment(endDate);

        let count = 0;
        let max = $scope.activeProducts.length;
        $scope.activeProducts.forEach( product => {
          product
            .timeSlots()
            .where({
              status: "bookable",
              startsAt: { 
                ge: startDate.toDate(), 
                le: endDate.toDate()
              }
            })
            .all()
            .then( timeSlots => {
              product.timeSlotsArray.push(...timeSlots.all());
              
              if(product.timeSlotsArray.length > 0){
                
                timeSlots.all().forEach( timeSlot => {
                  timeSlot.product = product;
                });

                $scope.masterActiveDays.push(...timeSlots.all());
              }else if(product.status == 'sold_out'){
                $scope.masterActiveDays.push({
                  product: product,
                  sold_out: true
                });
              }
              count++;

              //Resolve Condition
              if(count == max) {
                console.log("Done loading time slots");
                $scope.parseTimeSlots(true);
              }
            });
        });
      }
    }

    $scope.parseTimeSlots = function(shouldScroll){
      console.log("Parsing time lots");

      $scope.masterActiveDays.sort( (a, b) => {
        if(a.sold_out && b.sold_out){
          return moment(a.product.timeSlotsClosedBefore) - moment(b.product.timeSlotsClosedBefore);
        }
        if(a.sold_out){
          return moment(a.product.timeSlotsClosedBefore) - moment(b.startsAt);
        }
        if(b.sold_out){
          return moment(a.startsAt) - moment(b.product.timeSlotsClosedBefore);
        }
        if(!a.sold_out && !b.sold_out){
          return moment(a.startsAt) - moment(b.startsAt);
        }
      });

      console.log("Done sorting time slots");

      $scope.activeDays = _.groupBy($scope.masterActiveDays, (timeSlot) => {
        if(timeSlot.sold_out)
          return moment(timeSlot.product.timeSlotsClosedBefore).startOf('day').toDate();
        else
          return moment(timeSlot.startsAt).startOf('day').toDate();
      });

      console.log("Done grouping by day");

      for(var key in $scope.activeDays){
        $scope.activeDays[key] = _.groupBy($scope.activeDays[key], (timeSlot) => {
          return timeSlot.product.id;
        });
      }

      console.log("Done grouping by product");

      $scope.loadingProducts = false;
      $scope.loadingMoreDays = false;
      //$scope.multiSelectDisabled = false;
      $scope.$apply();

      console.log("Results displayed");

      if(shouldScroll) $scope.scrollToSearchDate();
    }

    $scope.scrollToSearchDate = function(){
      console.log("Scroll to search date", $scope.searchDate.startOf('day'));

      let unixSearchDate = $scope.searchDate.startOf('day').format('X');

      let dateHeaderElement = document.querySelector('#day-' + unixSearchDate);

      if(dateHeaderElement != null){
        $scope.listViewElement.scrollTop = $(dateHeaderElement).offset().top - 100;
      }else{
        let scrolled = false;
        $scope.masterActiveDays
          .sort( (a, b) => {
            return new moment(a.startsAt).unix() - new moment(b.startsAt).unix();
          })
          .forEach( (day, index) => {
            if(!scrolled){
              day_moment = new moment(day.startsAt).startOf('day');
              day_unix = day_moment.unix();

              if( day_moment.isBefore($scope.searchDate) ){
                if(index == $scope.masterActiveDays.length - 1 ){
                  dateHeaderElement = document.querySelector('#day-' + day_unix);
                  $scope.listViewElement.scrollTop = $(dateHeaderElement).offset().top - 100;
                  scrolled = true;
                  $scope.loadingScroll = false;
                }
              }else{
                dateHeaderElement = document.querySelector('#day-' + day_unix);
                $scope.listViewElement.scrollTop = $(dateHeaderElement).offset().top - 100;
                scrolled = true;
                $scope.loadingScroll = false;
              }
            }
          });
      }
    }

    $scope.formatDateHeader = function(date){
      return new moment( new Date(date) ).format('MMMM Do');
    }

    $scope.countOpenEvents = function(){
      if($scope.activeProducts != null){
        count = 0;
        $scope.activeProducts.forEach( product => {
          if(product.timeSlotsArray){
            if(product.timeSlotsArray.length > 0) count++;
          }
        });
        return count;
      }else{
        return 0;
      }
    }

    $scope.idFromDay = function(day_index){
      return new moment( new Date(day_index) ).format('X');
    }

    $scope.formatTimeSlot = function(timeSlots, day, product){
      let timeSlotsString = moment( new Date(day) ).format('MMM D');
      if(timeSlots.length)
        timeSlotsString += " | ";

      let count = 0;
      timeSlots.forEach( timeSlot => {
        if( moment( new Date(timeSlot.startsAt) ).isSame( new Date(day), 'day') ){
          if(count != 0) timeSlotsString += ', ';
          timeSlotsString += moment(timeSlot.startsAt).format('h:mmA');
          count++;
        }
      });
      return timeSlotsString;
    }

    $scope.formatDuration = function(product){
      let duration = product.timeSlotsArray[0].duration;
      if(duration == 60){
        return "1 Hour";
      }
      if(duration > 60){
        let durationString = '';
        let unit = null;
        let hoursOnly = Math.floor(duration / 60);
        let minutesOnly = duration % 60;

        if(hoursOnly == 1) unit = ' Hour';
        if(hoursOnly > 1) unit = ' Hours';

        durationString += hoursOnly + unit;

        if(minutesOnly != 0) durationString += ' ' + minutesOnly + ' Minutes';

        return durationString;
      }
      if(duration < 60){
        return duration + ' Minutes';
      }      
    }

  });

//Require Services
require('./services/occasionSDK.service');