var ActiveResource = require('active-resource');
var moment = require('moment');
var Occasion = require('occasion-sdk');

angular.module('PlayStreet')
    .factory('occasionSDKService', function($q) {

        this.listAllVenues = function() {
            window.VENUES.forEach(venue_item => {
                console.log("VENUE ITEM", venue_item);
                let occsnClient = new Occasion.Client({ token: venue_item.key });
                occsnClient.Merchant.first().then(merchant => { merchant.venues().all().then(venues => { console.log("VENUES", venues) }) }).catch(err => console.log("ERR", err));
            });
        }

        //Private Functions
        this.queryVenue = function(venue_item) {
            return new Promise( (resolve, reject) => {
                let this_occsn = new Occasion.Client({ token: venue_item.key });
                this_occsn.Venue
                    .find(venue_item.id)
                    .then( venue => {
                        venue.window_data = venue_item;
                        resolve(venue);
                    })
                    .catch( error => {
                        console.log("Venue/Products request error", error);
                        console.log("Error was with venue:", venue_item);
                    });
            });
        }

        this.queryAllVenues = function(){
            return new Promise( (resolve, reject) => {
                let venues = [];
                let promises = [];

                //Create promises to request each venue
                window.VENUES.forEach( (venue_item) => {
                    let promise = this.queryVenue(venue_item);
                    promises.push(promise);
                });

                //Call all promises together and return venue results
                Promise.all(promises).then( venues => resolve(venues) );
            });
        }

        //Return Public Member Variables and Functions
        return {
            getAllVenues: () => {
                return this.queryAllVenues();
            },
            listAllVenues: () => {
                return this.listAllVenues();
            }
        }

    });
